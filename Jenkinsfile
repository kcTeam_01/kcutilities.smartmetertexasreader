def commits = new ArrayList()
def CommitMessage = ''

pipeline {
    agent {
        label {
            label ''
            customWorkspace "workspace/${env.JOB_NAME}"
        }
    }
    environment {
        BUILD_VERSION = VersionNumber(projectStartDate: '1970-01-01', versionNumberString: '${BUILD_YEAR}.${BUILD_MONTH}.${BUILD_DAY}.${BUILDS_TODAY}', versionPrefix: '')
        SHORT_BUILD_VERSION = VersionNumber(projectStartDate: '1970-01-01', versionNumberString: '${BUILD_YEAR}.${BUILD_MONTH}.${BUILD_DAY}', versionPrefix: '')
        msbHome = tool name: 'VS Build Tools - Latest', type: 'msbuild'
    }
    stages {
        stage('Analyze changes') {
            steps {
                script {
                    def CommitInfo = getCommitInfo()

                    commits = CommitInfo.commits
                    CommitMessage = CommitInfo.CommitMessage
                }
            }
        }
		stage('Set Build Type') {
			parallel {
				stage('Production') {
					when { expression { return CommitMessage.startsWith("DeployProd"); } }
					steps {
						script {
							MyJob_PublishConfig = "Release"
							NugetVersion = BUILD_VERSION
						}
					}
				}
				stage('Development') {
					when { expression { return !CommitMessage.startsWith("DeployProd"); } }
					steps {
						script {
							MyJob_PublishConfig = "Debug"
							NugetVersion = BUILD_VERSION + '-beta'
						}
					}
				}
			}
		}
		stage('Show Build Values') {
			steps {
				script {
					echo "BUILD_VERSION: $BUILD_VERSION"
					echo "SHORT_BUILD_VERSION: $SHORT_BUILD_VERSION"
					echo "MyJob_PublishConfig: $MyJob_PublishConfig"
					echo "NugetVersion: $NugetVersion"
				}
			}
		}
        stage('Set Version Tag') {
            steps {
                script {
                    dir("kcUtilities.SmartMeterTexasReader") {
						def ProfFile = 'kcUtilities.SmartMeterTexasReader.csproj'
						def FileData = readFile(file: ProfFile, encoding: "UTF-8");
						FileData = FileData.replaceAll('<AssemblyVersion>1.0.1.0</AssemblyVersion>', "<AssemblyVersion>${BUILD_VERSION}</AssemblyVersion>")
						FileData = FileData.replaceAll('<FileVersion>1.0.1.0</FileVersion>', "<FileVersion>${BUILD_VERSION}</FileVersion>")
						FileData = FileData.replaceAll('<Version>1.0.1</Version>', "<Version>${NugetVersion}</Version>")
						writeFile file: ProfFile, text: FileData, encoding: "UTF-8"
                    }
                }
            }
        }
        stage('Nuget Restore') {
            steps {
                script {
                    dir("kcUtilities.SmartMeterTexasReader") {
                        sh(returnStdout: true, script: "dotnet restore")
                    }
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    dir("kcUtilities.SmartMeterTexasReader") {
						def ProfFile = 'kcUtilities.SmartMeterTexasReader.csproj'

                        sh(returnStdout: true, script: "dotnet build ${ProfFile} /p:Configuration=${MyJob_PublishConfig} /verbosity:minimal")
                    }
                }
            }
        }
        stage('Deploy') {
            steps {
                script {
                    dir("kcUtilities.SmartMeterTexasReader") {
						def ProfFile = 'kcUtilities.SmartMeterTexasReader.csproj'
                        sh(returnStdout: true, script: "dotnet pack ${ProfFile} --no-build --configuration ${MyJob_PublishConfig}")
                        
                        withCredentials([[$class: 'StringBinding', credentialsId: 'Nuget_org_KellCOMnet', variable: 'nugetKey']]) {
                            sh "dotnet nuget push \"${WORKSPACE}/kcUtilities.SmartMeterTexasReader/bin/${MyJob_PublishConfig}/*.nupkg\" --api-key ${env.nugetKey} --source https://api.nuget.org/v3/index.json"
                            echo "Pushed Nuget Package to repo"
                        }
                    }
                }
            }
        }
        stage('Cleanup') {
            steps {
                cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, cleanWhenUnstable: false, notFailBuild: true
            }
        }
    }
}