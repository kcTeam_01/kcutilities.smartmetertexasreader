
# README #

### What is this repository for? ###

The goal of this package is to provide a simple way to pull down the daily usage data from SmartMeterTexas.com 

### How do I get going? ###
1) To use this library you need to have an existing account with SmartMeterTexas.com

### BREAKING CHANGES 12/10/2019 ###
- As you are probably aware, SmartMeterTexas.com launched a new website on 12/10/2019. 
- - I have made the necessary changes to get this package working again in ALMOST that same manner but much more efficiently due to the rest API that is available now.
- You are welcome to keep using this package as it does some of the heavy lifting for you, but I am not exposing everything that the new API does, specifically metrics for power generator from wind mills/solar panels.
- If this breaks, please let me know, I am using this daily for my own usage but the fact that the new site asked for a security question concerns me.
- YOU MUST RUN THROUGH THE ONLINE ACCOUNT MIGRATION PROCESS BEFORE THIS WILL WORK!!!
- OK so what changes do you need to make to your programs?
--     Change ESID to ESIID (I made a typo orginally)
--     Change ReadLatest to Login


### Code Example ###

    using(var SMTR = new kcUtilities.SmartMeterTexasReader.Reader(account.Username, account.Password))
    {
        // You must call Login() before other calls
        // This does the actual login
        var currententries = SMTR.Login();

        var now = DateTime.Now;
        var startOfMonth = new DateTime(now.Year, now.Month, 1);

        var rundates = Range(startOfMonth.AddYears(-2), SMTR.LatestEndofDayRead_Date).ToList();
        rundates = rundates.Except(GetCurrentReadingDates(s_conn, SMTR.ESIID)).ToList();

        foreach(var rd in rundates)
        {
            // Get entries for all the dates in a list
            var rd_entries = SMTR.GetReadingsForDate(rd.Date);
        }
    }