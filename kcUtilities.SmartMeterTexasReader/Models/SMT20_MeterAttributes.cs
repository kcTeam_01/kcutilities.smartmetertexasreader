﻿namespace kcUtilities.SmartMeterTexasReader.Models
{
    public class SMT20_MeterAttribute_Dataum
    {
        #region Public Properties

        public int ampCls { get; set; }
        public string commInd { get; set; }
        public object crntTransRTO { get; set; }
        public int distGenChannel { get; set; }
        public string dscnct { get; set; }
        public string errmsg { get; set; }
        public string esiid { get; set; }
        public string frmWareVersion { get; set; }
        public object hanProtocol { get; set; }
        public string instDate { get; set; }
        public string instrmntRtd { get; set; }
        public int intSetting { get; set; }
        public string lastUpdated { get; set; }
        public string mtrMfr { get; set; }
        public string mtrMfrSrlNum { get; set; }
        public object mtrModel { get; set; }
        public int mtrMultplr { get; set; }
        public int mtrPhases { get; set; }
        public string mtrSTS { get; set; }
        public int numChannels { get; set; }
        public string provDate { get; set; }
        public object ptlTransRTO { get; set; }
        public string rvsFlow { get; set; }
        public string smtEngProfile { get; set; }
        public string tdspDunsNumber { get; set; }
        public string testDate { get; set; }
        public string utlMtrId { get; set; }

        #endregion Public Properties
    }

    public class SMT20_MeterAttributes
    {
        #region Public Properties

        public SMT20_MeterAttribute_Dataum[] data { get; set; }
        public string userType { get; set; }

        #endregion Public Properties
    }
}