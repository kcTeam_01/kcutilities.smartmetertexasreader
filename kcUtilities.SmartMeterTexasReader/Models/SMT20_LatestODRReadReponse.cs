﻿namespace kcUtilities.SmartMeterTexasReader.Models
{
    public class SMT20_LatestODRReadReponse
    {
        #region Public Properties

        public SMT20_LatestODRReadReponseData data { get; set; }

        #endregion Public Properties

        #region Public Classes

        public class SMT20_LatestODRReadReponseData
        {
            #region Public Properties

            public string odrdate { get; set; }
            public string odrread { get; set; }
            public string odrstatus { get; set; }
            public string odrusage { get; set; }
            public string responseMessage { get; set; }

            #endregion Public Properties
        }

        #endregion Public Classes
    }
}