﻿using Newtonsoft.Json;

namespace kcUtilities.SmartMeterTexasReader.Models
{
    internal class SMT20_UsageDataPoint
    {
        #region Public Properties

        [JsonProperty(PropertyName = "consumption")]
        public decimal Consumption { get; set; }

        [JsonProperty(PropertyName = "consumption_est_act")]
        public string Consumption_Type { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string Date { get; set; }

        [JsonProperty(PropertyName = "endtime")]
        public string EndTime { get; set; }

        [JsonProperty(PropertyName = "starttime")]
        public string StartTime { get; set; }

        #endregion Public Properties
    }
}