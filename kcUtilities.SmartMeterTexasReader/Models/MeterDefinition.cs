﻿using Newtonsoft.Json;

namespace kcUtilities.SmartMeterTexasReader.Models
{
    public class MeterDefinition
    {
        #region Public Properties

        [JsonProperty(PropertyName = "city")]
        public string Address_City { get; set; }

        [JsonProperty(PropertyName = "fullAddress")]
        public string Address_FullAddress { get; set; }

        [JsonProperty(PropertyName = "zip")]
        public string Address_Postal { get; set; }

        [JsonProperty(PropertyName = "state")]
        public string Address_State { get; set; }

        [JsonProperty(PropertyName = "address")]
        public string Address_StreetAddress { get; set; }

        [JsonProperty(PropertyName = "customer")]
        public string Customer { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "dunsNumber")]
        public string DUNS_Number { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "errmsg")]
        public string ErrorMessage { get; set; }

        [JsonProperty(PropertyName = "esiid")]
        public string ESIID { get; set; }

        [JsonProperty(PropertyName = "meterMultiplier")]
        public int MeterMultiplier { get; set; }

        [JsonProperty(PropertyName = "meterNumber")]
        public string MeterNumber { get; set; }

        [JsonProperty(PropertyName = "recordStatus")]
        public string RecordStatus { get; set; }

        [JsonProperty(PropertyName = "statusIndicator")]
        public bool StatusIndicator { get; set; }

        #endregion Public Properties
    }
}