﻿namespace kcUtilities.SmartMeterTexasReader.Models
{
    internal class SMT20_DashboardResponse
    {
        #region Public Properties

        public Data data { get; set; }

        #endregion Public Properties

        #region Public Classes

        public class Data
        {
            #region Public Properties

            public int count { get; set; }
            public MeterDetails defaultMeterDetails { get; set; }
            public bool generationFlag { get; set; }
            public string lastMeterReading { get; set; }
            public string latestDate { get; set; }
            public string meterReadDate { get; set; }
            public string reportPref { get; set; }
            public SMT20_UsageDataPoint[] usageData { get; set; }

            #endregion Public Properties
        }

        public class MeterDetails
        {
            #region Public Properties

            public string address { get; set; }
            public string description { get; set; }
            public string esiid { get; set; }
            public string fullAddress { get; set; }
            public int meterMultiplier { get; set; }
            public string meterNumber { get; set; }

            #endregion Public Properties
        }

        #endregion Public Classes
    }
}