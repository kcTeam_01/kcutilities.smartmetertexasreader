﻿using System;

namespace kcUtilities.SmartMeterTexasReader.Models
{
    public class DataEntry
    {
        #region Public Properties

        public DateTime EndTime { get; set; }
        public string ReadType { get; set; }
        public DateTime StartTime { get; set; }
        public Decimal Usage { get; set; }

        #endregion Public Properties
    }
}