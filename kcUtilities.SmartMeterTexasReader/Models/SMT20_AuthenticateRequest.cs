﻿using Newtonsoft.Json;

namespace kcUtilities.SmartMeterTexasReader.Models
{
    internal class SMT20_AuthenticateRequest
    {
        #region Public Properties

        [JsonProperty(Order = 1, PropertyName = "password")]
        public string password { get; set; }

        [JsonProperty(Order = 2, PropertyName = "rememberMe")]
        public string rememberMe { get; set; }

        [JsonProperty(Order = 0, PropertyName = "username")]
        public string username { get; set; }

        #endregion Public Properties
    }
}