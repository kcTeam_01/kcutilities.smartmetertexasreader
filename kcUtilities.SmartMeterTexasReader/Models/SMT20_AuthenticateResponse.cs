﻿namespace kcUtilities.SmartMeterTexasReader.Models
{
    internal class SMT20_AuthenticateResponse
    {
        #region Public Properties

        public string ai { get; set; }
        public string is_rgstrd { get; set; }
        public string lang_pref { get; set; }
        public string smtStatus { get; set; }
        public string smtStatuskey { get; set; }
        public string token { get; set; }
        public string usr_eml { get; set; }
        public string usr_frst_nm { get; set; }
        public string usr_id { get; set; }
        public string usr_lst_nm { get; set; }
        public string usr_type { get; set; }

        #endregion Public Properties
    }
}