﻿namespace kcUtilities.SmartMeterTexasReader.Models
{
    internal class SMT20_MeterListResponse
    {
        #region Public Properties

        public MeterDefinition[] data { get; set; }

        #endregion Public Properties
    }
}