﻿namespace kcUtilities.SmartMeterTexasReader.Models
{
    public class SMT20_SendOnDemandReadResponse
    {
        #region Public Properties

        public SMT20_SendOnDemandReadResponseData data { get; set; }

        #endregion Public Properties

        #region Public Classes

        public class SMT20_SendOnDemandReadResponseData
        {
            #region Public Properties

            public string correlationId { get; set; }
            public string statusCode { get; set; }
            public string statusReason { get; set; }
            public string trans_id { get; set; }

            #endregion Public Properties
        }

        #endregion Public Classes
    }
}