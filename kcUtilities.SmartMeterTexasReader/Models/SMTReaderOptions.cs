﻿namespace kcUtilities.SmartMeterTexasReader.Models
{
    public class SMTReaderOptions
    {
        #region Public Properties

        public string UserAgent { get; set; } = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36";
        public string UserAgentClientHint { get; set; } = "\"Google Chrome\";v=\"89\", \"Chromium\";v=\"89\", \";Not A Brand\";v=\"99\"";

        #endregion Public Properties
    }
}