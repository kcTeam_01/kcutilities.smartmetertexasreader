﻿namespace kcUtilities.SmartMeterTexasReader.Models
{
    internal class SMT20_SendOnDemandReadRequest
    {
        #region Public Properties

        public string ESIID { get; set; }
        public string MeterNumber { get; set; }

        #endregion Public Properties
    }
}