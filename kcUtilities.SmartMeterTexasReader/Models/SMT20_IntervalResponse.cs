﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace kcUtilities.SmartMeterTexasReader.Models
{
    internal class SMT20_IntervalResponse
    {
        #region Public Properties

        [JsonProperty(PropertyName = "generationFlag")]
        public bool GenerationFlag { get; set; }

        [JsonProperty(PropertyName = "intervaldata")]
        public List<SMT20_UsageDataPoint> IntervalData { get; set; }

        #endregion Public Properties
    }
}