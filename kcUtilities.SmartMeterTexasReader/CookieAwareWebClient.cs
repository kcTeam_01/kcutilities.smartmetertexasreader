﻿using kcUtilities.SmartMeterTexasReader.Models;
using System;
using System.Net;

namespace kcUtilities.SmartMeterTexasReader
{
    internal class CookieAwareWebClient : WebClient
    {
        #region Private Fields

        private readonly SMTReaderOptions _options;

        #endregion Private Fields

        #region Public Constructors

        public CookieAwareWebClient(SMTReaderOptions options)
        {
            _options = options;
        }

        #endregion Public Constructors

        #region Public Properties

        public CookieContainer CookieContainer { get; set; } = new CookieContainer();

        #endregion Public Properties

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest request = base.GetWebRequest(uri);
            if (request is HttpWebRequest hwr)
            {
                hwr.CookieContainer = CookieContainer;
                //hwr.Headers.Set(HttpRequestHeader.Pragma, "no-cache");
                //hwr.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                hwr.Headers.Set(HttpRequestHeader.AcceptLanguage, "en-US,en;q=0.9,ga;q=0.8,zh-TW;q=0.7,zh;q=0.6");
                hwr.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                hwr.Headers.Set("Sec-Fetch-Site", "same-origin");
                hwr.Headers.Set("Sec-Fetch-Mode", "cors");
                hwr.Headers.Set("Sec-Fetch-Dest", "empty");
                hwr.Headers.Set("sec-ch-ua-mobile", "?0");
                hwr.Headers.Set("sec-ch-ua", _options.UserAgentClientHint);
                hwr.Headers.Set("Origin", "https://www.smartmetertexas.com");
                hwr.UserAgent = _options.UserAgent;
                hwr.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            }
            return request;
        }

        #endregion Protected Methods
    }
}