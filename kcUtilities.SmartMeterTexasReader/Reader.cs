﻿using kcUtilities.SmartMeterTexasReader.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;

namespace kcUtilities.SmartMeterTexasReader
{
    public class Reader : IDisposable
    {
        #region Private Fields

        private readonly string _password;

        private readonly string _username;

        private readonly CookieAwareWebClient client;

        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// Initializing the read values, does not attempt login until ReadLatest() is called.
        /// Note: The account must already be created on SmartMeterTexas.com for this to have any chance of working.
        /// </summary>
        /// <param name="Username">The username to use for SmartMeterTexas Authentication</param>
        /// <param name="Password">The password to use for SmartMeterTexas Authentication</param>
        /// <param name="Cookies">The cookies to use for SmartMeterTexas Authentication</param>
        /// <param name="options">Different options that can be overriden for the requests.</param>
        public Reader(string Username, string Password, List<Cookie> Cookies, SMTReaderOptions options = null)
        {
            client = new CookieAwareWebClient(options ?? new SMTReaderOptions());
            foreach (var c in Cookies)
            {
                client.CookieContainer.Add(c);
            }
            _username = Username;
            _password = Password;
            Request_Initialize();
            GetMessages();
        }

        /// <summary>
        /// Initializing the read values, does not attempt login until ReadLatest() is called.
        /// Note: The account must already be created on SmartMeterTexas.com for this to have any chance of working.
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <param name="options">Different options that can be overriden for the requests.</param>
        public Reader(string Username, string Password, SMTReaderOptions options = null)
        {
            client = new CookieAwareWebClient(options ?? new SMTReaderOptions());
            _username = Username;
            _password = Password;
            Request_Initialize();
            GetMessages();
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// The default meter for the account.
        /// This is set during the Login process.
        /// </summary>
        public string ESIID { get; private set; }

        /// <summary>
        /// The date of the most recent meter read.
        /// This is set during the Login process.
        /// </summary>
        public DateTime LatestEndofDayRead_Date { get; private set; }

        /// <summary>
        /// The value of the default meter's last read.
        /// This is set during the Login process.
        /// </summary>
        public decimal LatestEndofDayRead_MeterReadValue { get; private set; }

        /// <summary>
        /// A list of the meters associated with the logged in account.
        /// This is set during the Login process.
        /// </summary>
        public ReadOnlyCollection<MeterDefinition> Meters { get; private set; }

        #endregion Public Properties

        #region Private Properties

        private SMT20_AuthenticateResponse AuthResponse { get; set; }

        private bool HasLoggedIn { get; set; }

        #endregion Private Properties

        #region Public Methods

        public void Dispose()
        {
            client.Dispose();
        }

        /// <summary>
        /// Gets the status/data of the last on demand read
        /// </summary>
        /// <param name="ESIID"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public SMT20_LatestODRReadReponse GetLatestOndemandRead(string ESIID)
        {
            if (!Meters.Any(p => p.ESIID == ESIID)) throw new ArgumentException($"ESIID '{ESIID}' is not in the list of valid ESIIDs.");
            client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
            client.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + AuthResponse.token);
            client.Headers.Set(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
            client.Headers.Set(HttpRequestHeader.Referer, "https://www.smartmetertexas.com/dashboard/");

            var data = new
            {
                ESIID
            };
            string body = JsonConvert.SerializeObject(data);

            byte[] postBytes = Encoding.UTF8.GetBytes(body);
            var bytes = client.UploadData("https://www.smartmetertexas.com/api/usage/latestodrread", postBytes);
            var ajax = Encoding.UTF8.GetString(bytes);

            return JsonConvert.DeserializeObject<SMT20_LatestODRReadReponse>(ajax);
        }

        /// <summary>
        /// Sends a request using the existing session to get the usage for the specified date for the default ESIID.
        /// </summary>
        /// <param name="ReadDate">The date to get values for, must be within the last year (Rolling 12) and be less than the Most Recent Meter Read (Use LatestEndofDayRead_Date)</param>
        /// <returns>A list of Models.DataEntry</returns>
        public List<DataEntry> GetReadingsForDate(DateTime ReadDate)
        {
            if (!HasLoggedIn) throw new InvalidOperationException("Must read current entries before accessing older records");
            return GetReadingsForDate(ESIID, ReadDate);
        }

        /// <summary>
        /// Sends a request using the existing session to get the usage for the specified date for the specified ESIID.
        /// </summary>
        /// <param name="ESIID">The specific ESIID to request data for. Your account must have the meter associated with it.</param>
        /// <param name="ReadDate">The date to get values for, must be within the last year (Rolling 12) and be less than the Most Recent Meter Read (Use LatestEndofDayRead_Date)</param>
        /// <returns>A list of Models.DataEntry</returns>
        public List<DataEntry> GetReadingsForDate(string ESIID, DateTime ReadDate)
        {
            if (!HasLoggedIn) throw new InvalidOperationException("Must read current entries before accessing older records");
            if (!Meters.Any(p => p.ESIID == ESIID)) throw new ArgumentException($"ESIID '{ESIID}' is not in the list of valid ESIIDs.");

            client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
            client.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + AuthResponse.token);
            client.Headers.Set(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
            client.Headers.Set(HttpRequestHeader.Referer, "https://www.smartmetertexas.com/dashboard/");

            var data = new
            {
                esiid = ESIID,
                startDate = ReadDate.ToString("MM/dd/yyyy"),
                endDate = ReadDate.ToString("MM/dd/yyyy"),
            };
            string body = JsonConvert.SerializeObject(data);

            byte[] postBytes = Encoding.UTF8.GetBytes(body);

            var bytes = client.UploadData("https://www.smartmetertexas.com/api/usage/interval", postBytes);
            var ajax = Encoding.UTF8.GetString(bytes);

            return JsonConvert.DeserializeObject<SMT20_IntervalResponse>(ajax).IntervalData.Select(p => new DataEntry()
            {
                StartTime = DateTime.Parse(p.Date + "  " + p.StartTime),
                EndTime = DateTime.Parse(p.Date + "  " + p.EndTime),
                ReadType = p.Consumption_Type,
                Usage = p.Consumption
            }).ToList();
        }

        /// <summary>
        /// Attempts to login then  populates the ESID and LatestReadValues and reads the default meter's latest date's values.
        /// </summary>
        /// <returns>A list of Models.DataEntry</returns>
        public List<DataEntry> Login()
        {
            AuthResponse = Request_Authenticate(client);

            if (AuthResponse != null)
            {
                var dashboard = Request_Dashboard();
                if (dashboard != null)
                {
                    HasLoggedIn = true;

                    DateTime.TryParse(dashboard.data.latestDate, out DateTime temp_LatestEndofDayRead_Date);
                    LatestEndofDayRead_Date = temp_LatestEndofDayRead_Date;

                    decimal.TryParse(dashboard.data.lastMeterReading, out decimal temp_lastMeterReading);
                    LatestEndofDayRead_MeterReadValue = temp_lastMeterReading;

                    ESIID = dashboard.data.defaultMeterDetails.esiid;

                    Meters = new ReadOnlyCollection<MeterDefinition>(RefreshMeterList());

                    return GetReadingsForDate(LatestEndofDayRead_Date);
                }
            }
            return null;
        }

        /// <summary>
        /// Requests the attibutes associated with a meter.
        /// </summary>
        /// <param name="ESIID">The specific ESIID to request data for. Your account must have the meter associated with it.</param>
        /// <returns>SMT20_MeterAttributes</returns>
        public SMT20_MeterAttributes ReadMeterAttributes(string ESIID)
        {
            if (!Meters.Any(p => p.ESIID == ESIID)) throw new ArgumentException($"ESIID '{ESIID}' is not in the list of valid ESIIDs.");

            client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
            client.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + AuthResponse.token);
            client.Headers.Set(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
            client.Headers.Set(HttpRequestHeader.Referer, "https://www.smartmetertexas.com/smartmeters/");

            var data = new
            {
                esiid = ESIID,
                isEsiid = "Y"
            };
            string body = JsonConvert.SerializeObject(data);

            byte[] postBytes = Encoding.UTF8.GetBytes(body);

            var bytes = client.UploadData("https://www.smartmetertexas.com/api/meter/meterattributes", postBytes);
            var ajax = Encoding.UTF8.GetString(bytes);

            return JsonConvert.DeserializeObject<SMT20_MeterAttributes>(ajax);
        }

        /// <summary>
        /// Sends a request to your meter for an on demand read.
        /// </summary>
        /// <param name="ESIID">The specific ESIID to request data for. Your account must have the meter associated with it.</param>
        /// <returns></returns>
        public SMT20_SendOnDemandReadResponse SendOnDemandRead(string ESIID)
        {
            if (!Meters.Any(p => p.ESIID == ESIID)) throw new ArgumentException($"ESIID '{ESIID}' is not in the list of valid ESIIDs.");
            client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
            client.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + AuthResponse.token);
            client.Headers.Set(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
            client.Headers.Set(HttpRequestHeader.Referer, "https://www.smartmetertexas.com/dashboard/");

            var data = new
            {
                ESIID,
                Meters.Single(p => p.ESIID == ESIID).MeterNumber
            };
            string body = JsonConvert.SerializeObject(data);

            byte[] postBytes = Encoding.UTF8.GetBytes(body);

            var bytes = client.UploadData("https://www.smartmetertexas.com/api/ondemandread", postBytes);
            var ajax = Encoding.UTF8.GetString(bytes);

            return JsonConvert.DeserializeObject<SMT20_SendOnDemandReadResponse>(ajax);
        }

        #endregion Public Methods

        #region Private Methods

        private void GetMessages()
        {
            try
            {
                client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
                client.Headers.Set(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
                client.Headers.Set(HttpRequestHeader.Referer, "https://www.smartmetertexas.com/home");

                var bytes = client.UploadData("https://www.smartmetertexas.com/api/serviceevents/messages/public", new byte[0]);
                /*     var text = Encoding.UTF8.GetString(bytes);

                     return JsonConvert.DeserializeObject<SMT20_AuthenticateResponse>(text);
                */
            }
            catch
            {
                //   throw;
            }
        }

        private List<MeterDefinition> RefreshMeterList()
        {
            if (!HasLoggedIn) throw new InvalidOperationException("Must read current entries before accessing older records");

            client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
            client.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + AuthResponse.token);
            client.Headers.Set(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
            client.Headers.Set(HttpRequestHeader.Referer, "https://www.smartmetertexas.com/smartmeters/");

            var data = new
            {
                esiid = "*",
            };
            string body = JsonConvert.SerializeObject(data);

            byte[] postBytes = Encoding.UTF8.GetBytes(body);

            var bytes = client.UploadData("https://www.smartmetertexas.com/api/meter", postBytes);
            var ajax = Encoding.UTF8.GetString(bytes);

            return JsonConvert.DeserializeObject<SMT20_MeterListResponse>(ajax).data.ToList();
        }

        private SMT20_AuthenticateResponse Request_Authenticate(CookieAwareWebClient client)
        {
            try
            {
                client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
                client.Headers.Set(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
                client.Headers.Set(HttpRequestHeader.Referer, "https://www.smartmetertexas.com/home");

                string body = JsonConvert.SerializeObject(new SMT20_AuthenticateRequest() { password = _password, rememberMe = "true", username = _username });
                byte[] postBytes = Encoding.UTF8.GetBytes(body);

                var bytes = client.UploadData("https://www.smartmetertexas.com/commonapi/user/authenticate", postBytes);
                var text = Encoding.UTF8.GetString(bytes);

                return JsonConvert.DeserializeObject<SMT20_AuthenticateResponse>(text);
            }
            catch
            {
                throw;
            }
        }

        private SMT20_DashboardResponse Request_Dashboard()
        {
            try
            {
                client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
                client.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + AuthResponse.token);
                client.Headers.Set(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
                client.Headers.Set(HttpRequestHeader.Referer, "https://www.smartmetertexas.com/dashboard/");

                byte[] postBytes = Encoding.UTF8.GetBytes(string.Empty);

                var bytes = client.UploadData("https://www.smartmetertexas.com/api/dashboard", postBytes);
                var ajax = Encoding.UTF8.GetString(bytes);

                return JsonConvert.DeserializeObject<SMT20_DashboardResponse>(ajax);
            }
            catch (WebException e)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void Request_Initialize()
        {
            try
            {
                client.Headers.Set(HttpRequestHeader.Accept, "application/json, text/plain, */*");
                var text = client.DownloadString("https://www.smartmetertexas.com/");
            }
            catch
            {
                throw;
            }
        }

        #endregion Private Methods
    }
}